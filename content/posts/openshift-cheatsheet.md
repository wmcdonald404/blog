---
title: Openshift Cheat Sheet
description: Cheat sheet of all things openshift
date: 2020-08-18
tags:
  - openshift
  - docker
---

Get inside the netnamespace of a running container - for use of tools like ping:

```bash
$ nsenter -u -n -i -p -t $(docker inspect --format "{{.State.Pid}}" <pod1-container-id>)
```


